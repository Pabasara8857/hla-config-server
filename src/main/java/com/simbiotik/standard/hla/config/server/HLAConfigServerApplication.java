package com.simbiotik.standard.hla.config.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class HLAConfigServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(HLAConfigServerApplication.class, args);
	}

}
