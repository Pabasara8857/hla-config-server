#the base image to be used
FROM adoptopenjdk/openjdk11:latest
LABEL maintainer="pabasara@simbiotiktech.com"

#docker build --no-cache=true --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') -t agents:latest .
ARG BUILD_DATE

ENV ACTIVE_CONFIG_SERVER_PROFILE="git"

#specify variables to be reused
ARG JAR_FILE=target/hla-config-server-0.0.1-SNAPSHOT.jar
				
#specific folder to be created in container
VOLUME /service_resources

LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.build-date=$BUILD_DATE
#files to be copied to container
ADD ${JAR_FILE} hla-config-server.jar

#the port of the container to be exposed
EXPOSE 8888

#Set variables to environment
ENV welcome.message="from docker config - hla-config-server"

#command and parameters that will be executed when a container runs
ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=${ACTIVE_CONFIG_SERVER_PROFILE}", "/hla-config-server.jar"]